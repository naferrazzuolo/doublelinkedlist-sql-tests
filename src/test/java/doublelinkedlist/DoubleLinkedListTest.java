package doublelinkedlist;

import org.capitole.doublelinkedlist.DoubleLinkedList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class DoubleLinkedListTest {

    @Test
    public void addTest() {
        int item1 = 100;
        String item2 = "New String";
        List<Integer> item3 = List.of(1, 2, 3);

        DoubleLinkedList<Object> actualList = new DoubleLinkedList<>();
        actualList.add(item1);
        actualList.add(item2);
        actualList.add(item3);

        Assertions.assertEquals(3, actualList.getSize());
    }

    @Test
    public void removeTest() {
        int item1 = 100;
        String item2 = "New String";
        List<Integer> item3 = List.of(1, 2, 3);

        DoubleLinkedList<Object> actualList = new DoubleLinkedList<>();
        actualList.add(item1);
        actualList.add(item2);
        actualList.add(item3);

        actualList.remove(item2);

        Assertions.assertEquals(2, actualList.getSize());

        // expected format [item1, [item3.1, item3.2, item3.3]]
        String expectedString = String.format("[%s, %s]", item1, item3);
        Assertions.assertEquals(expectedString, actualList.toString());
    }

    @Test
    public void removeFirstItemTest() {
        int item1 = 100;
        int item2 = 101;
        List<Integer> item3 = List.of(1, 2, 3);

        DoubleLinkedList<Object> actualList = new DoubleLinkedList<>();
        actualList.add(item1);
        actualList.add(item2);
        actualList.add(item3);

        actualList.remove(item1);

        Assertions.assertEquals(2, actualList.getSize());

        // expected format [item2, [item3.1, item3.2, item3.3]]
        String expectedString = String.format("[%s, %s]", item2, item3);
        Assertions.assertEquals(expectedString, actualList.toString());
    }

    @Test
    public void removeLatestItemTest() {
        int item1 = 100;
        int item2 = 101;
        List<Integer> item3 = List.of(1, 2, 3);

        DoubleLinkedList<Object> actualList = new DoubleLinkedList<>();
        actualList.add(item1);
        actualList.add(item2);
        actualList.add(item3);

        actualList.remove(item3);

        Assertions.assertEquals(2, actualList.getSize());

        // expected format [item1, item2]
        String expectedString = String.format("[%s, %s]", item1, item2);
        Assertions.assertEquals(expectedString, actualList.toString());
    }

    @Test
    public void removeOnlyOneInstanceItemTest() {
        int item1 = 100;
        String item2 = "New String";

        DoubleLinkedList<Object> actualList = new DoubleLinkedList<>();
        actualList.add(item1);
        actualList.add(item2);
        actualList.add(item1);

        actualList.remove(item1);

        Assertions.assertEquals(2, actualList.getSize());

        // expected format [item2, item1]
        String expectedString = String.format("[%s, %s]", item2, item1);
        Assertions.assertEquals(expectedString, actualList.toString());
    }

    @Test
    public void removeAllTest() {
        int item1 = 100;
        String item2 = "New String";
        int item3 = 201;

        DoubleLinkedList<Object> actualList = new DoubleLinkedList<>();
        actualList.add(item1);
        actualList.add(item2);
        actualList.add(item1);
        actualList.add(item1);
        actualList.add(item3);

        actualList.removeAll(item1);

        Assertions.assertEquals(2, actualList.getSize());

        // expected format [item2, item3]
        String expectedString = String.format("[%s, %s]", item2, item3);
        Assertions.assertEquals(expectedString, actualList.toString());
    }

    @Test
    public void getTest() {
        int item1 = 100;
        String item2 = "New String";
        List<Integer> item3 = List.of(1, 2, 3);
        int item4 = 400;
        int item5 = 500;

        DoubleLinkedList<Object> actualList = new DoubleLinkedList<>();
        actualList.add(item1);
        actualList.add(item2);
        actualList.add(item3);
        actualList.add(item4);
        actualList.add(item5);

        Assertions.assertEquals(item1, actualList.get(0));
        Assertions.assertEquals(item2, actualList.get(1));
        Assertions.assertEquals(item3, actualList.get(2));
        Assertions.assertEquals(item4, actualList.get(3));
        Assertions.assertEquals(item5, actualList.get(4));
    }

    @Test
    public void toArrayTest() {
        int item1 = 100;
        String item2 = "New String";
        List<Integer> item3 = List.of(1, 2, 3);

        DoubleLinkedList<Object> actualList = new DoubleLinkedList<>();
        actualList.add(item1);
        actualList.add(item2);
        actualList.add(item3);

        Object[] expectedArray = new Object[]{item1, item2, item3};
        Object[] actualArray = actualList.toArray();

        Assertions.assertEquals(expectedArray[0], actualArray[0]);
        Assertions.assertEquals(expectedArray[1], actualArray[1]);
        Assertions.assertEquals(expectedArray[2], actualArray[2]);
    }

    @Test
    public void toStringTest() {
        int item1 = 100;
        String item2 = "New String";
        List<Integer> item3 = List.of(1, 2, 3);

        DoubleLinkedList<Object> actualList = new DoubleLinkedList<>();
        actualList.add(item1);
        actualList.add(item2);
        actualList.add(item3);

        // expected format [item1, item2, [item3.1, item3.2, item3.3]]
        String expectedString = String.format("[%s, %s, %s]", item1, item2, item3);
        Assertions.assertEquals(expectedString, actualList.toString());
        System.out.println("expected: " + expectedString + " - actual: " + actualList);
    }

    @Test
    public void isEmptyTest() {
        DoubleLinkedList<Integer> actualList = new DoubleLinkedList<>();
        Assertions.assertEquals(Boolean.TRUE, actualList.isEmpty());
    }

    @Test
    public void isNotEmptyTest() {
        DoubleLinkedList<Integer> actualList = new DoubleLinkedList<>();
        actualList.add(100);

        Assertions.assertEquals(Boolean.FALSE, actualList.isEmpty());
    }

}
