package org.capitole.doublelinkedlist;

import java.util.Objects;

public class DoubleLinkedList<T> {

    private DoubleNode<T> first;
    private DoubleNode<T> last;
    private int size;

    public DoubleLinkedList() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    /**
     * Add a new element at the end of the list
     *
     * @param data | object|data to add to the list
     */
    public void add(T data) {
        DoubleNode<T> node = new DoubleNode<>(data);
        if (this.isEmpty()) {
            this.first = node;
        } else {
            this.last.setNext(node);
            node.setPrevious(this.last);
        }
        this.last = node;
        ++this.size;
    }

    /**
     * Remove only the first item in the list that contains the specified data
     *
     * @param data | object|data to remove from the list
     */
    public void remove(T data) {
        if (this.isEmpty()) {
            return; // nothing to remove
        }

        // if there is only one element in the list and is the element to remove, empty the list
        if (this.size == 1 && Objects.equals(this.first.getData(), data)) {
            this.emptyList();
            return;
        }

        DoubleNode<T> current = this.first;
        while (current != null) {
            if (Objects.equals(current.getData(), data)) {
                this.removeNode(current);
                break;
            }
            current = current.getNext();
        }
    }


    /**
     * Remove all the elements in the list that contains the specified data
     *
     * @param data | object|data to remove from the list
     */
    public void removeAll(T data) {
        if (this.isEmpty()) {
            return; // nothing to remove
        }

        // if there is only one element in the list and is the element to remove, empty the list
        if (this.size == 1 && Objects.equals(this.first.getData(), data)) {
            this.emptyList();
            return;
        }

        DoubleNode<T> current = this.first;
        while (current != null) {
            if (Objects.equals(current.getData(), data)) {
                this.removeNode(current);
            }
            current = current.getNext();
        }
    }

    private void removeNode(DoubleNode<T> node) {
        // take the node of the previous element of 'current' and point it's next to the next of 'current'
        if (node.getPrevious() != null) {
            node.getPrevious().setNext(node.getNext());
        } else {
            this.first = node.getNext(); // if previous is null, then current is the first one and must reassign first variable
        }
        // take the node of the next element of 'current' and point it's previous to the previous of 'current'
        if (node.getNext() != null) {
            node.getNext().setPrevious(node.getPrevious());
        } else {
            this.last = node.getPrevious(); // if next is null, then current is the last one and must reassign last variable
        }
        --size;
    }

    private void emptyList() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    /**
     * Get an element in the `index` position of the list
     *
     * @param index | position of the element into the list
     * @return the element at position `index`
     */
    public T get(int index) {
        if (index < 0 || index >= this.getSize()) {
            return null;
        }

        DoubleNode<T> current;
        // if index is closer to the end than the beginning of the list then traverse in reverse
        if (index > this.getSize() / 2) {
            current = this.last;
            for (int x = (this.getSize() - 1); x > index; x--) {
                current = current.getPrevious();
            }
        } else {
            current = this.first;
            for (int x = 0; x < index; x++) {
                current = current.getNext();
            }
        }
        return current.getData();
    }

    /**
     * @return the list as an array
     */
    @SuppressWarnings("unchecked")
    public T[] toArray() {
        T[] array = (T[]) new Object[this.getSize()];
        DoubleNode<T> current = this.first;

        for (int x=0; x < this.getSize(); x++) {
            array[x] = current.getData();
            current = current.getNext();
        }
        return array;
    }

    /**
     * @return Return the list as String in format Array.toString() [item1, item2, item3]
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("[");
        DoubleNode<T> current = this.first;
        while (current != null) {
            stringBuilder.append(current.getData());
            if (current.getNext() != null) {
                stringBuilder.append(", ");
            }
            current = current.getNext();
        }
        return stringBuilder.append("]").toString();
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public int getSize() {
        return this.size;
    }
}
