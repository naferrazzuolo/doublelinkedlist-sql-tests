package org.capitole.doublelinkedlist;

public class DoubleNode<T> {

    private final T data;
    private DoubleNode<T> previous;
    private DoubleNode<T> next;


    // Insert at the end of the list
    public DoubleNode(T data) {
        this(data, null, null);
    }

    // Insert at the beginning of the list
    public DoubleNode(T data, DoubleNode<T> previous, DoubleNode<T> next) {
        this.data = data;
        this.previous = previous;
        this.next = next;
    }

    public T getData() {
        return this.data;
    }

    public DoubleNode<T> getNext() {
        return this.next;
    }

    public void setNext(DoubleNode<T> node) {
        this.next = node;
    }

    public DoubleNode<T> getPrevious() {
        return this.previous;
    }

    public void setPrevious(DoubleNode<T> node) {
        this.previous = node;
    }
}
