package org.capitole.testsql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;

/**
 * Mejorar cada uno de los métodos a nivel SQL y código cuando sea necesario
 * Razonar cada una de las mejoras que se han implementado
 * No es necesario que el código implementado funcione
 */
public class TestSqlDao {

    private static TestSqlDao instance;

    private TestSqlDao() {

    }

    public static TestSqlDao getInstance() {
        if (instance == null) {
            instance = new TestSqlDao();
        }
        return instance;
    }

    /**
     * Obtiene el ID del último pedido para cada usuario
     */
    public Hashtable<Long, Long> getMaxUserOrderId(long idTienda) throws Exception {

        Connection connection = null;
        PreparedStatement selectStatement = null;

        Hashtable<Long, Long> maxOrderUser = new Hashtable<>();
        String selectQuery = "SELECT MAX(ID_PEDIDO) AS ID_PEDIDO, ID_USUARIO FROM PEDIDOS WHERE ID_TIENDA = ? GROUP BY ID_USUARIO";

        try {
            connection = getConnection();
            selectStatement = connection.prepareStatement(selectQuery);
            selectStatement.setLong(1, idTienda);
            ResultSet rs = selectStatement.executeQuery();

            while (rs.next()) {
                long idPedido = rs.getInt("ID_PEDIDO");
                long idUsuario = rs.getInt("ID_USUARIO");
                maxOrderUser.put(idUsuario, idPedido);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (selectStatement != null) {
                selectStatement.close();
            }
        }

        return maxOrderUser;
    }

    /**
     * Copia todos los pedidos de un usuario a otro
     */
    public void copyUserOrders(long idUserOri, long idUserDes) throws Exception {

        Connection connection = null;
        PreparedStatement selectStatement = null;
        PreparedStatement insertStatement = null;

        String selectQuery = "SELECT FECHA, TOTAL, SUBTOTAL, DIRECCION FROM PEDIDOS WHERE ID_USUARIO = ?";

        try {
            connection = getConnection();
            selectStatement = connection.prepareStatement(selectQuery);
            selectStatement.setLong(1, idUserOri);
            ResultSet rs = selectStatement.executeQuery();

            connection.setAutoCommit(false);

            String insertQuery = "INSERT INTO PEDIDOS (ID_USUARIO, FECHA, TOTAL, SUBTOTAL, DIRECCION) VALUES (?, ?, ?, ?, ?)";
            insertStatement = connection.prepareStatement(insertQuery);
            insertStatement.setLong(1, idUserDes);

            while (rs.next()) {
                insertStatement.setTimestamp(2, rs.getTimestamp("FECHA"));
                insertStatement.setDouble(3, rs.getDouble("TOTAL"));
                insertStatement.setDouble(4, rs.getDouble("SUBTOTAL"));
                insertStatement.setString(5, rs.getString("DIRECCION"));

                insertStatement.addBatch(insertQuery);
            }

            insertStatement.executeBatch();
            connection.commit();

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (selectStatement != null) {
                selectStatement.close();
            }
            if (insertStatement != null) {
                insertStatement.close();
            }
        }

    }

    /**
     * Obtiene los datos del usuario y pedido con el pedido de mayor importe para la tienda dada
     * TODO Retornar entidad Usuario
     */
    public void getUserMaxOrder(long idTienda, long userId, long orderId, String name, String address) throws Exception {

        Connection connection = null;
        PreparedStatement selectStatement = null;

        String selectQuery = "SELECT U.ID_USUARIO, P.ID_PEDIDO, P.TOTAL, U.NOMBRE, U.DIRECCION FROM PEDIDOS AS P "
                + "INNER JOIN USUARIOS AS U ON P.ID_USUARIO = U.ID_USUARIO "
                + "WHERE P.ID_TIENDA = ?"
                + "ORDER BY P.TOTAL DESC LIMIT 1";  // get the order with the highest amount only

        try {
            connection = getConnection();
            selectStatement = connection.prepareStatement(selectQuery);
            selectStatement.setLong(1, idTienda);
            ResultSet rs = selectStatement.executeQuery();
            double total = 0;

            if (rs.next()) {
                total = rs.getLong("TOTAL");
                userId = rs.getInt("ID_USUARIO");
                orderId = rs.getInt("ID_PEDIDO");
                name = rs.getString("NOMBRE");
                address = rs.getString("DIRECCION");
            }

            // must return here an entity object like 'Usuario'

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (selectStatement != null) {
                selectStatement.close();
            }
        }
    }

    private Connection getConnection() {

        // return JDBC connection
        return null;
    }
}
