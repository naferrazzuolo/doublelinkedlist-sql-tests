# DoubleLinkedList-SQL-Tests

Desarrollo de ejercicios de prueba java para Capitole:
1. Implementación de lista doblemente enlazada con tests unitarios.
2. Optimización de código Java y SQL presentes en la clase TestSqlDao.


- Para el **Test1** se ha realizado una clase DoubleLinkedList, ubicado en el path 'src/main/java/org.capitole/doublelinkedlist/DoubleLinkedList.java'
y su correspondiente test unitario con JUnit ubicado en 'src/test/java/doublelinkedlist/DoubleLinkedListTest.java'

- Para el **Test2** se ha insertado tal cual fue entregado el archivo TestSqlDao.java y se ha realizado cambios progresivos para poder distinguir las diferentes mejoras.
El mismo se encuentra en el path 'src/main/java/org/capitole/testsql/TestSqlDao.java'
Cabe aclarar que no se ha integrado ninguna BD al proyecto.
